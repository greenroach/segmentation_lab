#include "stdafx.h"
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/ml/ml.hpp>
#include <iostream>

using std::cout;
using std::cin;
using std::endl;
using namespace cv;

Mat getSobel(Mat& src);
Mat getWatershed(Mat& src);
Mat getKmean(Mat src, int clusterCount = 2);
Mat getGrubCut(Mat src);

std::string srcWinName; //��� ���� � ������������ ������������
Mat currImage; //������� ����������� ��� ��������� ������
Rect rect; //������� ������������� ���������� ��� ����� ����

//#define LOOP ����������������� ��� ������ ������ ������ �� �������, ���� ��������������� ��� ������� ����������� ������


/**
 * \brief ���������� ������� ����. ������ ������� � ����� ����� �� �����������.
 * \param event
 * \param x
 * \param y
 * \param flags
 * \param userdata
 */
void callBackFunc(int event, int x, int y, int flags, void* userdata) {
	
	static Mat src = currImage; //��������� �������� �����������

	const int RECT_SIZE = 100;
	const int RECT_SIZEx2 = RECT_SIZE * 2;
	
	//���� ������ ����� ������ ����
	if (event == EVENT_LBUTTONDOWN) {
		//������� ��������� �������� ���, ����� ������� �������� 
		//�� �������� �� ������� �����������
		
		int w = currImage.cols;
		int h = currImage.rows;
		int x1, x2, y1, y2;

		if (x < 0 + RECT_SIZE) {
			x1 = 0;
			x2 = RECT_SIZEx2;
		}
		else if (x > w - RECT_SIZE) {
			x1 = x - RECT_SIZEx2;
			x2 = w;
		}
		else {
			x1 = x - RECT_SIZE;
			x2 = x + RECT_SIZE;
		}

		if (y < 0 + RECT_SIZE) {
			y1 = RECT_SIZEx2;
			y2 = 0;
		}
		else if (y > h - RECT_SIZE) {
			y1 = w;
			y2 = y - RECT_SIZEx2;
		}
		else {
			y1 = y + RECT_SIZE;
			y2 = y - RECT_SIZE;
		}
		
		//�������� ����������� ������������
		currImage = src.clone();
		//������ ����� �������
		rect = Rect(x1, y2, RECT_SIZEx2, RECT_SIZEx2);
		rectangle(currImage, rect, cv::Scalar(255, 191, 0), 2);
		//���������� ����� �����������
		imshow(srcWinName, currImage);
	}
}

int main()
{
	auto src = cv::imread("src.png");
	srcWinName = "origin";
	cv::imshow(srcWinName, src);
	
	std::string winname = std::string();
	
	int a = 2; //������ ����� ��� ������ � ����������� ������
	Mat out;

#ifdef LOOP
	waitKey(1);
	cout << "Command list :" << endl;
	cout << "\t1: Sobel:" << endl;
	cout << "\t2: Watershed:" << endl;
	cout << "\t3: Threshold:" << endl;
	cout << "\t4: Adaptive threshold:" << endl;
	cout << "\t5: K-Mean:" << endl;
	cout << "\t6: Grub Cut:" << endl;
	while (true) {
		cout << "Enter command:" << endl;
		cin >> a;
#endif

		if (a == 1) {
			out = getSobel(src);
			winname = "sobel";
		}

		if (a == 2) {
			out = getWatershed(src);
			winname = "watershed";
		}

		if (a == 3) {
			Mat gray;
			cvtColor(src, gray, CV_RGB2GRAY);
			threshold(gray, out, 100, 250, CV_THRESH_BINARY);
			winname = "threshold";
		}

		if (a == 4) {
			Mat gray;
			cvtColor(src, gray, CV_RGB2GRAY);
			adaptiveThreshold(gray, out, 255, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 21, 1);
			winname = "adaptive threshold";
		}

		if (a == 5) {
			Mat gray;
			cvtColor(src, gray, CV_RGB2GRAY);
			out = getKmean(gray);
			winname = "k-means";
		}

		if (a == 6) {
			winname = "grubCut";
			out = getGrubCut(src);
		}

		imshow(winname, out);
		waitKey();
#ifdef LOOP
	}
#endif
	return 0;
}

/**
* \brief ����������� ������ ����������� �������� �����
* \param src �������� �����������
* \return ���������������� �����������
*/
Mat getGrubCut(Mat src) {
	currImage = src.clone();

	//������������� ������� ���� �� ����� � ������������ ������������
	setMouseCallback(srcWinName, callBackFunc, NULL);
	//���� ������� �� �������
	waitKey();
	//������� ������� ����
	setMouseCallback(srcWinName, NULL, NULL);

	Mat result;
	Mat bgModel, fgModel;

	cout << "grubCut begin ..." << endl;
	grabCut(src, result, rect, bgModel, fgModel, 1, cv::GC_INIT_WITH_RECT);
	cout << "grubCut success ..." << endl;

	//������� �����
	compare(result, cv::GC_PR_FGD, result, cv::CMP_EQ);
	cv::Mat foreground(src.size(), CV_8UC3, cv::Scalar(255, 255, 255));

	//�������� ������ �� ����
	src.copyTo(foreground, result);
	return result;
}

/**
* \brief ����������� ������� �-�������
* \param src �������� �����������
* \param clusterCount = 2 ���������� ���������
* \return ���������������� �����������
*/
Mat getKmean(Mat src, const int clusterCount) {

	Mat labels;
	const int attempts = 5;
	Mat centers;

	//����������� ����������� NxM � ������ ���������  N*Mx1
	Mat samples(src.rows * src.cols, 1, CV_32F);
	for (int y = 0; y < src.rows; y++)
		for (int x = 0; x < src.cols; x++) {
			samples.at<float>(y + x*src.rows, 0) = src.at<uchar>(y, x);
		}

	//������������� ������� �-�������
	kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 100, 0.001), attempts, KMEANS_RANDOM_CENTERS, centers);

	//����������� ������ ���������  N*Mx1 � ����������� NxM
	Mat new_image(src.size(), src.type());
	for (int y = 0; y < src.rows; y++)
		for (int x = 0; x < src.cols; x++) {
			const int cluster_idx = labels.at<int>(y + x*src.rows, 0);
			new_image.at<uchar>(y, x) = centers.at<float>(cluster_idx, 0);
		}
	return new_image;
}

/**
 * \brief ����������� ������� �����������
 * \param src �������� �����������
 * \return ���������������� �����������
 */
Mat getWatershed(Mat& src) {
	
	const Mat image = src.clone();
	currImage = src.clone();
	setMouseCallback(srcWinName, callBackFunc, nullptr);
	waitKey();
	setMouseCallback(srcWinName, nullptr, nullptr);

	// ������� �������������
	cv::Mat markers(image.size(), CV_32SC1, cv::Scalar(-1));
	//������� ����
	markers(Rect(0, 0, image.cols, 5)) = Scalar::all(1);
	//������ �������������
	markers(Rect(0, image.rows - 5, image.cols, 5)) = Scalar::all(1);
	//����� �������������
	markers(Rect(0, 0, 5, image.rows)) = Scalar::all(1);
	//������ �������������
	markers(Rect(image.cols - 5, 0, 5, image.rows)) = Scalar::all(1);
	//����������� �������������
	markers(rect) = Scalar::all(2);

	//��� ������� ������ �������
	//markers.convertTo(markers, CV_BGR2GRAY); 
	//imshow("markers", markers);
	//waitKey();

	cout << "watershed begin ..." << endl;
	watershed(image, markers);
	cout << "watershed end ..." << endl;
	Mat dst = Mat(src.size(), CV_8U, Scalar::all(0));
	//������������ ������� � ����������� �� �������������� �������
	for (int i = 0; i < markers.rows; i++) {
		for (int j = 0; j < markers.cols; j++) {
			const int index = markers.at<int>(i, j);
			if (index == -1)
				dst.at<uint8_t>(i, j) = 0; //�������
			else if (index == 1)
				dst.at<uint8_t>(i, j) = 100; //���
			else
				dst.at<uint8_t>(i, j) = 255; //������
		}
	}

	return dst;
}

/**
* \brief ����������� ������� ��������� ������ (�������� ������)
* \param src �������� �����������
* \return ���������������� �����������
*/
Mat getSobel(Mat& src) {

	const int scale = 1;
	const int delta = 0;
	const int ddepth = CV_16S;
	Mat grad, gray;

	//��������� � ������� ������
	cvtColor(src, gray, CV_RGB2GRAY);

	//���������� grad_x � grad_y
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;

	// �������� �� X
	Sobel(gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_x, abs_grad_x);

	// �������� �� Y
	Sobel(gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
	convertScaleAbs(grad_y, abs_grad_y);

	// ����� ��������
	addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
	return grad;
}
